package com.epam.view;

import com.epam.controller.PaintVarnishProductController;
import com.epam.controller.PlumbingProductController;
import com.epam.controller.WoodenProductController;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

/**
 * Represent the view of the application.
 *
 * @author Oleh Ferents
 */
public class Menu {
    /**
     * Name of menu.
     */
    private String name;

    /**
     * Description text of menu.
     */
    private String text;

    /**
     * Storage of menu items and methods.
     */
    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    /**
     * Constructor for initializing menu items and controllers.
     * @param paintVarnishProductController - {@link PaintVarnishProductController}
     * @param plumbingProductController - {@link PlumbingProductController}
     * @param woodenProductController - {@link WoodenProductController}
     */
    public Menu(final PaintVarnishProductController paintVarnishProductController,
                final PlumbingProductController plumbingProductController,
                final WoodenProductController woodenProductController) {

        Menu menu = new Menu("Supermarket", "Main menu");

        Menu plumbingProductsMenu = new Menu(
                "Supermarket",
                "Plumbing products menu");

        plumbingProductsMenu.putAction(
                "Get all plumbing products",
                plumbingProductController::printAll);

        plumbingProductsMenu.putAction(
                "Get products by price range",
                plumbingProductController::printAllWhenPriceInRange);

        plumbingProductsMenu.putAction(
                "Back",
                () -> menu.
                        activateMenu(menu));

        Menu paintVarnishProductsMenu = new Menu(
                "Supermarket",
                "Paint and varnish products menu");

        paintVarnishProductsMenu.putAction(
                "Get all paint and varnish products",
                paintVarnishProductController::printAll);

        paintVarnishProductsMenu.putAction(
                "Get products by producer",
                paintVarnishProductController::printAllByProducer);

        paintVarnishProductsMenu.putAction(
                "Back",
                () -> menu.
                        activateMenu(menu));

        Menu woodenProductsMenu = new Menu("Supermarket", "Wooden products menu");

        woodenProductsMenu.putAction(
                "Get all wooden products",
                woodenProductController::printAll);

        woodenProductsMenu.putAction(
                "Get products by color",
                woodenProductController::printAllByColor);

        woodenProductsMenu.putAction(
                "Back",
                () -> menu.
                        activateMenu(menu));

        menu.putAction(
                "Open plumbing products menu",
                () -> plumbingProductsMenu
                        .activateMenu(plumbingProductsMenu));

        menu.putAction(
                "Open paint and varnish products menu",
                () -> paintVarnishProductsMenu.
                        activateMenu(paintVarnishProductsMenu));

        menu.putAction(
                "Open wooden products menu",
                () -> woodenProductsMenu.
                        activateMenu(woodenProductsMenu));

        menu.putAction(
                "Exit",
                () -> System.exit(0));

        menu.activateMenu(menu);
    }

    /**
     * Method that starts menu and receive user console input.
     * @param menu - menu instance to show
     */
    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    /**
     * Method for adding menu item.
     * @param name - name of item
     * @param action - method that will be called
     */
    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    /**
     * Method that builds menu using {@link StringBuilder}.
     * @return string with all menu items
     */
    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    /**
     * Method for calling action by user input.
     * @param actionNumber - user console input
     */
    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}
