package com.epam;

import com.epam.controller.PaintVarnishProductController;
import com.epam.controller.PlumbingProductController;
import com.epam.controller.WoodenProductController;
import com.epam.controller.impl.PaintVarnishProductControllerImpl;
import com.epam.controller.impl.PlumbingProductControllerImpl;
import com.epam.controller.impl.WoodenProductControllerImpl;
import com.epam.model.dao.PaintVarnishProductDao;
import com.epam.model.dao.PlumbingProductDao;
import com.epam.model.dao.WoodenProductDao;
import com.epam.model.dao.impl.PaintVarnishProductDaoImpl;
import com.epam.model.dao.impl.PlumbingProductDaoImpl;
import com.epam.model.dao.impl.WoodenProductDaoImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        PaintVarnishProductDao paintVarnishProductDao =
                new PaintVarnishProductDaoImpl();
        PaintVarnishProductController paintVarnishProductController =
                new PaintVarnishProductControllerImpl(paintVarnishProductDao);

        PlumbingProductDao plumbingProductDao =
                new PlumbingProductDaoImpl();
        PlumbingProductController plumbingProductController =
                new PlumbingProductControllerImpl(plumbingProductDao);

        WoodenProductDao woodenProductDao =
                new WoodenProductDaoImpl();
        WoodenProductController woodenProductController =
                new WoodenProductControllerImpl(woodenProductDao);

        new Menu(
                paintVarnishProductController,
                plumbingProductController,
                woodenProductController
        );
    }
}
