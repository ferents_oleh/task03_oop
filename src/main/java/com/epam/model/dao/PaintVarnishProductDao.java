package com.epam.model.dao;

import com.epam.model.domain.PaintVarnishProduct;

import java.util.List;

/**
 * Interface that implement Data Access Object pattern for {@link PaintVarnishProduct}.
 *
 * @author Oleh Ferents
 */
public interface PaintVarnishProductDao {
    /**
     *
     * @return list of {@link PaintVarnishProduct}
     */
    List<PaintVarnishProduct> getAll();

    /**
     * Method for receiving filtered list of paint and varnishes products
     * by product field.
     * @param producer - producer(country) of product
     * @return list of filtered {@link PaintVarnishProduct}
     */
    List<PaintVarnishProduct> getAllByProducer(String producer);
}
