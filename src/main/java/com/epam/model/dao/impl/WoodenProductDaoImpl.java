package com.epam.model.dao.impl;

import com.epam.model.dao.WoodenProductDao;
import com.epam.model.domain.Door;
import com.epam.model.domain.Laminate;
import com.epam.model.domain.WoodenProduct;

import java.util.ArrayList;
import java.util.List;

public class WoodenProductDaoImpl implements WoodenProductDao {
    private List<WoodenProduct> woodenProducts;

    public WoodenProductDaoImpl() {
        init();
    }

    private void init() {
        woodenProducts = new ArrayList<>();
        List<WoodenProduct> data = new ArrayList<>() {
            {
                add(new Door(
                        "OpenTeck ELIT",
                        7400,
                        12,
                        "Центр ЛТД",
                        "білий",
                        2400,
                        1500,
                        2,
                        "МДФ",
                        "засклені"
                ));
                add(new Laminate(
                        "Kronopol",
                        164,
                        18,
                        "ТД Україна",
                        "бежевий",
                        1380,
                        193,
                        6,
                        "дуб",
                        "лак",
                        "багатосмугова"
                ));
                add(new Door(
                        "Zimen Z-07",
                        4900,
                        16,
                        "Оселя Буд",
                        "коричневий",
                        2030,
                        850,
                        1,
                        "МДФ",
                        "сталь"
                ));
            }
        };

        woodenProducts.addAll(data);
    }

    @Override
    public final List<WoodenProduct> getAll() {
        return woodenProducts;
    }

    @Override
    public final List<WoodenProduct> getAllByColor(String color) {
        List<WoodenProduct> result = new ArrayList<>();
        color = color.toLowerCase();
        for (WoodenProduct woodenProduct : woodenProducts) {
            String productColor = woodenProduct.getColor();
            productColor = productColor.toLowerCase();
            if (color.equals(productColor)) {
                result.add(woodenProduct);
            }
        }
        return result;
    }
}
