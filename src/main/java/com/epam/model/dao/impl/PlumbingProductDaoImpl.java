package com.epam.model.dao.impl;

import com.epam.model.dao.PlumbingProductDao;
import com.epam.model.domain.PlumbingProduct;
import com.epam.model.domain.Sink;
import com.epam.model.domain.Toilet;

import java.util.ArrayList;
import java.util.List;

public class PlumbingProductDaoImpl implements PlumbingProductDao {
    private List<PlumbingProduct> plumbingProducts;

    public PlumbingProductDaoImpl() {
        init();
    }

    private void init() {
        plumbingProducts = new ArrayList<>();

        List<PlumbingProduct> data = new ArrayList<>() {
            {
                add(new Toilet("EASY 001",
                        3900,
                        12,
                        "Cersanit",
                        780,
                        360,
                        630,
                        "підлоговий",
                        "нижній",
                        "дюропласт"
                ));
                add(new Toilet("FACILE 011",
                        3300,
                        5,
                        "Cersanit",
                        795,
                        335,
                        625,
                        "підлоговий",
                        "нижній",
                        "дюропласт"
                ));
                add(new Sink("Ovale",
                        3100,
                        8,
                        "DEVIT",
                        200,
                        545,
                        400,
                        "врізна",
                        "білий"
                ));
            }
        };

        plumbingProducts.addAll(data);
    }

    @Override
    public final List<PlumbingProduct> getAll() {
        return plumbingProducts;
    }

    @Override
    public final List<PlumbingProduct> getAllByPriceRange(
            final int priceFrom,
            final int priceTo) {
        List<PlumbingProduct> filteredList = new ArrayList<>();
        for (PlumbingProduct plumbingProduct : plumbingProducts) {
            double price = plumbingProduct.getPrice();
            if (priceFrom <= price && price <= priceTo) {
                filteredList.add(plumbingProduct);
            }
        }
        return filteredList;
    }
}
