package com.epam.model.dao.impl;

import com.epam.model.dao.PaintVarnishProductDao;
import com.epam.model.domain.Paint;
import com.epam.model.domain.PaintVarnishProduct;
import com.epam.model.domain.Varnish;

import java.util.ArrayList;
import java.util.List;

public class PaintVarnishProductDaoImpl implements PaintVarnishProductDao {
    private List<PaintVarnishProduct> paintVarnishProducts;

    public PaintVarnishProductDaoImpl() {
        init();
    }

    private void init() {
        paintVarnishProducts = new ArrayList<>();

        List<PaintVarnishProduct> data = new ArrayList<>() {
            {
                add(new Paint(
                        "SI-25",
                        45,
                        3,
                        "SMILE PAINTS",
                        "бетон",
                        "для внутрішніх робіт",
                        "білосніжний",
                        "акрилова"
                ));
                add(new Varnish(
                        "AQUA INTERIOR",
                        330,
                        2,
                        "KOMPOZIT",
                        "дерево",
                        "паркетний",
                        "помаранчевий",
                        "глянсовий"
                ));
            }
        };

        paintVarnishProducts.addAll(data);
    }

    @Override
    public final List<PaintVarnishProduct> getAll() {
        return paintVarnishProducts;
    }

    @Override
    public final List<PaintVarnishProduct> getAllByProducer(String producer) {
        producer = producer.toLowerCase();
        List<PaintVarnishProduct> result = new ArrayList<>();
        for (PaintVarnishProduct paintVarnishProduct : paintVarnishProducts) {
            String productProducer = paintVarnishProduct.getProducer();
            productProducer = productProducer.toLowerCase();
            if (productProducer.equals(producer)) {
                result.add(paintVarnishProduct);
            }
        }
        return result;
    }
}
