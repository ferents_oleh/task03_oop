package com.epam.model.dao;

import com.epam.model.domain.WoodenProduct;

import java.util.List;

/**
 * Interface that implement Data Access Object pattern for {@link WoodenProduct}.
 *
 * @author Oleh Ferents
 */
public interface WoodenProductDao {
    /**
     * Method for receiving list of wooden products.
     * @return list of {@link WoodenProduct}
     */
    List<WoodenProduct> getAll();

    /**
     * Method for receiving filtered list of wooden products by color.
     * @param color - color of product
     * @return filtered list of {@link WoodenProduct}
     */
    List<WoodenProduct> getAllByColor(String color);
}
