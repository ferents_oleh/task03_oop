package com.epam.model.dao;

import com.epam.model.domain.PlumbingProduct;

import java.util.List;

/**
 * Interface that implement Data Access Object pattern for {@link PlumbingProduct}.
 *
 * @author Oleh Ferents
 */
public interface PlumbingProductDao {
    /**
     *  Method for receing list of plumbing products.
     * @return list of {@link PlumbingProduct}
     */
    List<PlumbingProduct> getAll();

    /**
     * Method for receiving filtered list of plumbing products
     * by price range.
     * @param priceFrom - start of price range
     * @param priceTo - end of price range
     * @return list of filtered {@link PlumbingProduct}
     */
    List<PlumbingProduct> getAllByPriceRange(int priceFrom, int priceTo);
}
