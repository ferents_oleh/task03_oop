package com.epam.model.domain;

public class Toilet extends PlumbingProduct {
    private String waterSupply;

    private String coverMaterial;

    public Toilet(final String name,
                  final double price,
                  final int quantity,
                  final String producer,
                  final double height,
                  final double width,
                  final double depth,
                  final String installationMethod,
                  final String waterSupply,
                  final String coverMaterial) {
        super(
                name,
                price,
                quantity,
                producer,
                height,
                width,
                depth,
                installationMethod);
        this.waterSupply = waterSupply;
        this.coverMaterial = coverMaterial;
    }

    public final String getWaterSupply() {
        return waterSupply;
    }

    public final void setWaterSupply(final String waterSupply) {
        this.waterSupply = waterSupply;
    }

    public final String getCoverMaterial() {
        return coverMaterial;
    }

    public final void setCoverMaterial(final String coverMaterial) {
        this.coverMaterial = coverMaterial;
    }

    @Override
    public final String toString() {
        return "Toilet{" +
                "waterSupply='" + waterSupply + '\'' +
                ", coverMaterial='" + coverMaterial + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", depth=" + depth +
                ", installationMethod='" + installationMethod + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", producer='" + producer + '\'' +
                '}';
    }
}
