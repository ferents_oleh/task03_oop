package com.epam.model.domain;

public abstract class PaintVarnishProduct extends Product {
    private String materialForPainting;

    private String scope;

    private String color;

    public PaintVarnishProduct(final String name,
                               final double price,
                               final int quantity,
                               final String producer,
                               final String materialForPainting,
                               final String scope,
                               final String color) {
        super(name, price, quantity, producer);
        this.materialForPainting = materialForPainting;
        this.scope = scope;
        this.color = color;
    }

    public final String getMaterialForPainting() {
        return materialForPainting;
    }

    public final PaintVarnishProduct setMaterialForPainting(final String materialForPainting) {
        this.materialForPainting = materialForPainting;
        return this;
    }

    public final String getScope() {
        return scope;
    }

    public final PaintVarnishProduct setScope(final String scope) {
        this.scope = scope;
        return this;
    }

    public final String getColor() {
        return color;
    }

    public final PaintVarnishProduct setColor(final String color) {
        this.color = color;
        return this;
    }
}
