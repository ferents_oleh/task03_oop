package com.epam.model.domain;

public class Varnish extends PaintVarnishProduct {
    private String glossDegree;

    public Varnish(final String name,
                   final double price,
                   final int quantity,
                   final String producer,
                   final String materialForPainting,
                   final String scope,
                   final String color,
                   final String glossDegree) {
        super(
                name,
                price,
                quantity,
                producer,
                materialForPainting,
                scope,
                color);
        this.glossDegree = glossDegree;
    }

    public final String getGlossDegree() {
        return glossDegree;
    }

    public final void setGlossDegree(final String glossDegree) {
        this.glossDegree = glossDegree;
    }

    @Override
    public final String toString() {
        return "Varnish{" +
                "glossDegree='" + glossDegree + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", producer='" + producer + '\'' +
                '}';
    }
}
