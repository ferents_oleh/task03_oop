package com.epam.model.domain;

public class Laminate extends WoodenProduct {
    private double depth;

    private String texture;

    private String coating;

    private String lath;

    public Laminate(final String name,
                    final double price,
                    final int quantity,
                    final String producer,
                    final String color,
                    final double height,
                    final double width,
                    final double depth,
                    final String texture,
                    final String coating,
                    final String lath) {
        super(name, price, quantity, producer, color, height, width);
        this.depth = depth;
        this.texture = texture;
        this.coating = coating;
        this.lath = lath;
    }

    public final double getDepth() {
        return depth;
    }

    public final void setDepth(final double depth) {
        this.depth = depth;
    }

    public final String getTexture() {
        return texture;
    }

    public final void setTexture(final String texture) {
        this.texture = texture;
    }

    public final String getCoating() {
        return coating;
    }

    public final void setCoating(final String coating) {
        this.coating = coating;
    }

    public final String getLath() {
        return lath;
    }

    public final void setLath(final String lath) {
        this.lath = lath;
    }

    @Override
    public final String toString() {
        return "Laminate{" +
                "depth=" + depth +
                ", texture='" + texture + '\'' +
                ", coating='" + coating + '\'' +
                ", lath='" + lath + '\'' +
                ", color='" + color + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", producer='" + producer + '\'' +
                '}';
    }
}
