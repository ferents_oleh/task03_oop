package com.epam.model.domain;

public abstract class Product {
    protected String name;
    protected double price;
    protected int quantity;
    protected String producer;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final double getPrice() {
        return price;
    }

    public final void setPrice(final double price) {
        this.price = price;
    }

    public final int getQuantity() {
        return quantity;
    }

    public final void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public final String getProducer() {
        return producer;
    }

    public final void setProducer(final String producer) {
        this.producer = producer;
    }

    public Product(final String name, final double price, final int quantity,final  String producer) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.producer = producer;
    }

    protected double calculateTotalPrice() {
        return price * quantity;
    };
}
