package com.epam.model.domain;

public abstract class WoodenProduct extends Product {
    protected String color;

    protected double height;

    protected double width;

    public WoodenProduct(
            final String name,
            final double price,
            final int quantity,
            final String producer,
            final String color,
            final double height,
            final double width) {
        super(name, price, quantity, producer);
        this.color = color;
        this.height = height;
        this.width = width;
    }

    public final String getColor() {
        return color;
    }

    public final void setColor(final String color) {
        this.color = color;
    }

    public final double getHeight() {
        return height;
    }

    public final void setHeight(final double height) {
        this.height = height;
    }

    public final double getWidth() {
        return width;
    }

    public final void setWidth(final double width) {
        this.width = width;
    }
}
