package com.epam.model.domain;

public abstract class PlumbingProduct extends Product {
    protected double height;
    protected double width;
    protected double depth;
    protected String installationMethod;

    public final double getHeight() {
        return height;
    }

    public final void setHeight(final double height) {
        this.height = height;
    }

    public final double getWidth() {
        return width;
    }

    public final void setWidth(final double width) {
        this.width = width;
    }

    public final double getDepth() {
        return depth;
    }

    public final void setDepth(final double depth) {
        this.depth = depth;
    }

    public final String getInstallationMethod() {
        return installationMethod;
    }

    public final void setInstallationMethod(final String installationMethod) {
        this.installationMethod = installationMethod;
    }

    public PlumbingProduct(final String name,
                           final double price,
                           final int quantity,
                           final String producer,
                           final double height,
                           final double width,
                           final double depth,
                           final String installationMethod) {
        super(name, price, quantity, producer);
        this.height = height;
        this.width = width;
        this.depth = depth;
        this.installationMethod = installationMethod;
    }
}
