package com.epam.model.domain;

public class Paint extends PaintVarnishProduct {
    private String paintBase;

    public Paint(final String name,
                 final double price,
                 final int quantity,
                 final String producer,
                 final String materialForPainting,
                 final String scope,
                 final String color,
                 final String paintBase) {
        super(
                name,
                price,
                quantity,
                producer,
                materialForPainting,
                scope,
                color);
        this.paintBase = paintBase;
    }

    public final String getPaintBase() {
        return paintBase;
    }

    public final void setPaintBase(final String paintBase) {
        this.paintBase = paintBase;
    }

    @Override
    public final String toString() {
        return "Paint{" +
                "paintBase='" + paintBase + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", producer='" + producer + '\'' +
                '}';
    }
}
