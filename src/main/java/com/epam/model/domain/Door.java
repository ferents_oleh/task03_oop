package com.epam.model.domain;

public class Door extends WoodenProduct {
    private int quantityOfDoorLeafs;

    private String material;

    private String construction;

    public Door(final String name,
                final double price,
                final int quantity,
                final String producer,
                final String color,
                final double height,
                final double width,
                final int quantityOfDoorLeafs,
                final String material,
                final String construction) {
        super(name, price, quantity, producer, color, height, width);
        this.quantityOfDoorLeafs = quantityOfDoorLeafs;
        this.material = material;
        this.construction = construction;
    }

    public final int getQuantityOfDoorLeafs() {
        return quantityOfDoorLeafs;
    }

    public final void setQuantityOfDoorLeafs(final int quantityOfDoorLeafs) {
        this.quantityOfDoorLeafs = quantityOfDoorLeafs;
    }

    public final String getMaterial() {
        return material;
    }

    public final void setMaterial(final String material) {
        this.material = material;
    }

    public final String getConstruction() {
        return construction;
    }

    public final void setConstruction(final String construction) {
        this.construction = construction;
    }

    @Override
    public final String toString() {
        return "Door{" +
                "quantityOfDoorLeafs=" + quantityOfDoorLeafs +
                ", material='" + material + '\'' +
                ", construction='" + construction + '\'' +
                ", color='" + color + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", producer='" + producer + '\'' +
                '}';
    }
}
