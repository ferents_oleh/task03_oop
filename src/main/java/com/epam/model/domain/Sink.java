package com.epam.model.domain;

public class Sink extends PlumbingProduct {
    private String color;

    public Sink(final String name,
                final double price,
                final int quantity,
                final String producer,
                final double height,
                final double width,
                final double depth,
                final String installationMethod,
                final String color) {
        super(
                name,
                price,
                quantity,
                producer,
                height,
                width,
                depth,
                installationMethod);
        this.color = color;
    }

    public final String getColor() {
        return color;
    }

    public final void setColor(final String color) {
        this.color = color;
    }

    @Override
    public final String toString() {
        return "Sink{" +
                "color='" + color + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", depth=" + depth +
                ", installationMethod='" + installationMethod + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", producer='" + producer + '\'' +
                '}';
    }
}
