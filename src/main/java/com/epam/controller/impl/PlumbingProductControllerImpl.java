package com.epam.controller.impl;

import com.epam.controller.PlumbingProductController;
import com.epam.model.dao.PlumbingProductDao;

import java.util.Scanner;

/**
 * Implementation of {@link PlumbingProductController} interface.
 */
public class PlumbingProductControllerImpl implements PlumbingProductController {
    private PlumbingProductDao plumbingProductDao;

    public PlumbingProductControllerImpl(
            final PlumbingProductDao plumbingProductDao) {
        this.plumbingProductDao = plumbingProductDao;
    }

    @Override
    public final void printAll() {
        plumbingProductDao
                .getAll()
                .forEach(p -> System.out.println(p.toString()));
    }

    @Override
    public final void printAllWhenPriceInRange() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter price range: ");

        int priceFrom = scanner.nextInt();
        int priceTo = scanner.nextInt();

        plumbingProductDao
                .getAllByPriceRange(priceFrom, priceTo)
                .forEach(p -> System.out.println(p.toString()));
    }
}
