package com.epam.controller.impl;

import com.epam.controller.PaintVarnishProductController;
import com.epam.model.dao.PaintVarnishProductDao;
import com.epam.model.domain.PaintVarnishProduct;

import java.util.List;
import java.util.Scanner;

/**
 * implementation of {@link PaintVarnishProductController} interface.
 */
public class PaintVarnishProductControllerImpl implements PaintVarnishProductController {
    private PaintVarnishProductDao paintVarnishProductDao;

    public PaintVarnishProductControllerImpl(
            final PaintVarnishProductDao paintVarnishProductDao) {
        this.paintVarnishProductDao = paintVarnishProductDao;
    }

    @Override
    public final void printAll() {
        List<PaintVarnishProduct> products = paintVarnishProductDao.getAll();
        products.forEach(p -> System.out.println(p.toString()));
    }

    @Override
    public final void printAllByProducer() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter producer - ");
        String producer = scanner.next();
        paintVarnishProductDao
                .getAllByProducer(producer)
                .forEach(p -> System.out.println(p.toString()));
    }
}
