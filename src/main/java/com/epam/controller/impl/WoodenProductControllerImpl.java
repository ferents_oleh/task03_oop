package com.epam.controller.impl;

import com.epam.controller.WoodenProductController;
import com.epam.model.dao.WoodenProductDao;

import java.util.Scanner;

/**
 * Implementation of {@link WoodenProductController} interface.
 */
public class WoodenProductControllerImpl implements WoodenProductController {
    private WoodenProductDao woodenProductDao;

    public WoodenProductControllerImpl(
            final WoodenProductDao woodenProductDao) {
        this.woodenProductDao = woodenProductDao;
    }

    @Override
    public final void printAll() {
        woodenProductDao
                .getAll()
                .forEach(w -> System.out.println(w.toString()));
    }

    @Override
    public final void printAllByColor() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a color - ");

        String color = scanner.next();

        woodenProductDao
                .getAllByColor(color)
                .forEach(w -> System.out.println(w.toString()));
    }
}
