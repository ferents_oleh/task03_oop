package com.epam.controller;

/**
 * Interface for pass data from
 * {@link com.epam.model.domain.WoodenProduct} model to view.
 *
 * @author Oleh Ferents
 */
public interface WoodenProductController {
    /**
     * Method for printing all wooden products.
     */
    void printAll();

    /**
     * Method for printing all wooden products filtered by color.
     */
    void printAllByColor();
}
