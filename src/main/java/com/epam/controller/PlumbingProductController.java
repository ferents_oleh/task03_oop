package com.epam.controller;

/**
 * Interface for pass data from
 * {@link com.epam.model.domain.PlumbingProduct} model to view.
 *
 * @author Oleh Ferents
 */
public interface PlumbingProductController {
    /**
     * Method for printing all plumbing products.
     */
    void printAll();

    /**
     * Method for printing plumbing products filtered by price range.
     */
    void printAllWhenPriceInRange();
}
