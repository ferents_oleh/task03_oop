package com.epam.controller;

/**
 * Interface for pass data from
 * {@link com.epam.model.domain.PaintVarnishProduct} model to view.
 *
 * @author Oleh Ferents
 */
public interface PaintVarnishProductController {
    /**
     * Method for printing all paint and varnish products.
     */
    void printAll();

    /**
     * Method for printing paint and varnish products filtered by producer.
     */
    void printAllByProducer();
}
